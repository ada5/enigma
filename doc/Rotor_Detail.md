
# Rotor Detail

Unless otherwise noted, details and directions are as seen
by the operator position.

The rotor is basically a 3-part mechanism and has the following
characteristics (directions as seen by operator):

* Mechanism for stepping rotor(s)
    * There are 2 styles of stepping mechanisms - the pawl
    and the cogwheel. Only 2 models of the Enigma are known
    to have cogwheel style stepping mechanism.
    * The pawl style typically had the index notch as part
    of the ring, while the cogwheel style used a different
    stepping mechanism.
    * Typical rotors moved in a counter-clockwise direction
    as seen from the ETW (right) side of the rotor.
    * Abwehr (G-Series) machines the 4th wheel stepped as
    well due to mechanical differences from other models.

* Hub with wiring dependent on customer request
    * There were 2 styles of known rotor wiring - 26 position
    (standard) and 28 position (typically Swedish/Swiss version).
    * There were standard wiring schemes for the majority of
    customers, the actual wiring can be specified by the
    customer. Some customers ordered blank rotors that would
    be set by the customer.

* Display ring that can be set independent of rotor position
    * Normally, the ring slid over the hub, reducing complexity
    while allowing per-message, user-defined start position(s)
    independent of actual pins.
    * Changing the ring position did not alter the wiring,
    but did alter the starting position of the rotor encryption
    cycle.
    * Pawl-style rotor notches were part of the ring, so changing
    the ring setting altered the notch settings in relation to
    the rotor pins.
    * Normal Enigma machines used letters on the ring - all commercial
    customers prior to WWII and Naval M1 .. M4 machines. German
    Army adopted Enigma machines with numbers only.

## Wiring Types

There are basically 3 types of wiring available for the Enigma
machine areas:

1. F2B (Front-to-Back) - Signal comes in one side and exits on the
opposite side
    * Signal input pin can be redirected to any signal output pin
2. F2F (Front-to-Front) - Signal comes in one side and exits on
the same side
    * Signal input pin must exit on a different signal output pin
3. Plugboard - F2B with restrictions

Basic design areas are:

* ETW - Standard ETW is F2B with no pin swapping. Some models designed
with input pins mapping to different output pins similar in function to
rotors.
* Rotors - F2B with all input pins mapped to different output pins.
* UKW - Since output is on the same side as input, no signal path
can map input pin to the same output pin. Used to redirect the signal
back through (or "reflect") the signal through machine.

## Stepping

There are 2 styles of mechanics involved in the Enigma
machine - cogwheels and pawls.

Cogwheel models used missing teeth in the rotor gear
as the stepping mechanism. Gears were part of the hub
and not the ring, so stepping was not altered
on these rotors when setting the display ring to an
offset position.

The pawl notch was a dip in the rotor that the lever
could drop into for stepping the next rotor in sequence.
When the notch was part of the display ring, the offset
would apparently change the stepping position, but could
be compensated for since the offset was linear in action.

### Pawl

The pawl style of rotor mechanics were present in the early models
of the Enigma machines until the Enigma D model A28.

The rotor construction specified a notch ring on the left,
the wiring (along with the engraved symbols) in the middle,
and a ratchet on the right. The number of teeth on the rachet
corresponded to the number of pins on the rotor.

The right rotor ratchet was engaged at all times, making the right
rotor step at every press of a key. The middle rotor ratchet was
engaged whenever a notch was present on the right notch ring.
The left rotor rachet was engaged whenever a notch was present
on the middle notch ring.

All levers moved whenever a key was pressed. Since all levers
moved at the same time, whenever a notch was encountered on any
pawl style rotor, the appropriate rachet was also engaged.
Due to this quirk of the pawl system, whenever the notch is
encountered on the middle rotor, the next press of any key
also cause the pawl lever to engage both the left and middle
rotors, causing the 'double-step' effect.

A .. F - Letters shown on rotor during stepping

N - Notch on rotor

| Step | Left | Middle<br />Notch | Middle | Right<br />Notch | Right |
| :-: | :-: | :-: | :-: | :-: | :-: |
| 1   | A   |     | A   |     | A   |
| 2   | A   |     | A   |     | B   |
| 3   | A   |     | A   | N   | C   |
| 4   | A   | N   | B   |     | D   |
| 5   | B   |     | C   |     | E   |
| 6   | B   |     | C   |     | F   |

Although some models had 4 rotors visible, all pawl
style Enigma's only had pawls between left-middle rotors,
middle-right rotors, and right rotor-ETW. The fourth rotor
(far left not shown) can be set, but did not have pawls
and did not step.

Left rotor was a standard rotor with a notch, but since there
were no pawls/levers on the left side of the left rotor, no stepping
beyond the left rotor is available.

### Cogwheel

The Enigma D model A28 was a modified and updated version of the Enigma D,
also known as the Z&#228;hlwerk Enigma. The follow-on Enigma with cogwheels
was the Enigma G (Enigma G31).

The cogwheel versions of Enigma did not have the double-step
issue of the pawl version, plus had the added benefit of being
able to be stepped backwards to accomodate corrections via
a removeable gear handle.

Although only 3 wheels normally stepped, some versions of the cogwheel
style allowed stepping of other parts (like UKW and/or 4th rotor),
not just the middle rotors.

## Display Ring

The display ring caused a variation in the encryption sequence
by altering the starting position of the rotor.

For the pawl style where notches were part of the ring,
this would appear to also affect the stepping sequence,
but could be accounted for in decrypting since it was a
linear translation rather than a change to the wiring
itself - it only caused a variation for the initial
setting of the rotor, but the signal path was unchanged.

Most pawl style models had the notch as part of the display
ring, but select models attached the notch to the body rather
than the display ring.

The cogwheel style had the gaps in the gear that was not
affected by the display ring. On this style, the display
ring offset only affected the initial position setting
and did not change the wiring or stepping effects.

The display ring was normally engraved with letters.
Selected models and those used by the German Army
were engraved with numbers. See the specifications
of the specific models for actual engraving used.

### Standard Rotors

These rotors routed the input from the right side of the wheel to the output
on the left side of the wheel - essentially remapping the input pin to a
different output pin.

* I .. V - Standard rotor variations.
* VI .. VIII - Extra rotors (typically available for M4 only)
* Beta, Gamma - Thin wheels only available for the M4. Used with
the UKW-B and UKW-C thin versions of the reflector wheel.

### Reflectors

These rotors routed the wiring from the input side of the wheel
back into a different pin on the input side. Although this arrangement
made it harder to decrypt the letter transpositions, it also had
the effect of reducing the cryptographic space, thereby
reducing the permutations available. A key weakness was a letter
could not be encrypted back to itself, causing the reduced
cryptographic strength of the cypher.

* UKW - The standard "reflector" wheel. Not compatible with
the M4 due to physical size.
* UKW-B, UKW-C - The M4 used the thinner B and C
versions with the Beta and Gamma 4th wheels.
* UKW-D - Limited version that was field rewireable. Not
compatible with the M4 due to physical size.

### Signal Path

The signal path is basically routed between the chassis wiring and the
rotor internal wiring.

* Pin 1 chassis wiring never changes
* Pin 1 rotor wiring changes in relation to chassis wiring every keypress
* Although rotor wiring will redirect output to a different pin, programming
for chassis pin <-> rotor pin will remain the same

Basic signal path chassis pin 1 (A) through rotor to chassis pin 1 (forward)
and chassis pin 3 (C) to chassis pin 3 (reflected):

| Chassis out letter | Chassis out pin | Signal Rotor Out | Rotor out pin | Rotor letter | Rotor in pin | Signal Rotor In | Chassis in pin | Chassis in letter |
| :-: | :-: | :--: | :-: | :-: | :-: | :--: | :-: | :-: |
|  Z  |  26 |      |  26 |  Z  |  26 |      |  26 |  Z  |
|  A  |  1  | <--- |  1  |  A  |  1  | <--- |  1  |  A  |
|  B  |  2  |      |  2  |  B  |  2  |      |  2  |  B  |
|  C  |  3  | ---> |  3  |  C  |  3  | ---> |  3  |  C  |
|  D  |  4  |      |  4  |  D  |  4  |      |  4  |  D  |

Signal path after 1 step:

| Chassis out letter | Chassis out pin | Signal Rotor Out | Rotor out pin | Rotor letter | Rotor in pin | Signal Rotor In | Chassis in pin | Chassis in letter |
| :-: | :-: | :--: | :-: | :-: | :-: | :--: | :-: | :-: |
|  Z  |  26 |      |  1  |  A  |  1  |      |  26 |  Z  |
|  A  |  1  | <--- |  2  |  B  |  2  | <--- |  1  |  A  |
|  B  |  2  |      |  3  |  C  |  3  |      |  2  |  B  |
|  C  |  3  | ---> |  4  |  D  |  4  | ---> |  3  |  C  |
|  D  |  4  |      |  5  |  E  |  5  |      |  4  |  D  |

Signal path after 2 steps:

| Chassis out letter | Chassis out pin | Signal Rotor Out | Rotor out pin | Rotor letter | Rotor in pin | Signal Rotor In | Chassis in pin | Chassis in letter |
| :-: | :-: | :--: | :-: | :-: | :-: | :--: | :-: | :-: |
|  Z  |  26 |      |  2  |  B  |  2  |      |  26 |  Z  |
|  A  |  1  | <--- |  3  |  C  |  3  | <--- |  1  |  A  |
|  B  |  2  |      |  4  |  D  |  4  |      |  2  |  B  |
|  C  |  3  | ---> |  5  |  E  |  5  | ---> |  3  |  C  |
|  D  |  4  |      |  6  |  F  |  6  |      |  4  |  D  |

Signal path after 3 steps:

| Chassis out letter | Chassis out pin | Signal Rotor Out | Rotor out pin | Rotor letter | Rotor in pin | Signal Rotor In | Chassis in pin | Chassis in letter |
| :-: | :-: | :--: | :-: | :-: | :-: | :--: | :-: | :-: |
|  Z  |  26 |      |  3  |  C  |  3  |      |  26 |  Z  |
|  A  |  1  | <--- |  4  |  D  |  4  | <--- |  1  |  A  |
|  B  |  2  |      |  5  |  E  |  5  |      |  2  |  B  |
|  C  |  3  | ---> |  6  |  F  |  6  | ---> |  3  |  C  |
|  D  |  4  |      |  7  |  G  |  7  |      |  4  |  D  |

* For programming purposes, rotor position (offset from chassis pin 1)
must be accounted for in both entering and leaving the rotor(s)
