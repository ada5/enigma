# Translation Tables

Defines tables used to translate between letters and Enigma encryption paths.

Currently, there are 3 types of translations:

* Standard Enigma            - 26 pins/26 letters
* Swedish Enigma             - 28 pins/28 letters ("W" not used)
* Funkschl&#252;ssel Enigma  - 28 pins/29 letters ("X" unencrypted)

## General Translation Table

The following Latin_1 characters are locally renamed
to allow easier translation for Enigma use:


| Latin_1 Character[^1] | Enigma Use     |
| -------------------- | --------------- |
| NUL                  | NUL             |
| SOH                  | Pin_01          |
| STX                  | Pin_02          |
| ETX                  | Pin_03          |
| EOT                  | Pin_04          |
| ENQ                  | Pin_05          |
| ACK                  | Pin_06          |
| BEL                  | Pin_07          |
| BS                   | Pin_08          |
| HT                   | Pin_09          |
| LF                   | Pin_10          |
| VT                   | Pin_11          |
| FF                   | Pin_12          |
| CR                   | Pin_13          |
| SO                   | Pin_14          |
| SI                   | Pin_15          |
| DLE                  | Pin_16          |
| DC1                  | Pin_17          |
| DC2                  | Pin_18          |
| DC3                  | Pin_19          |
| DC4                  | Pin_20          |
| NAK                  | Pin_21          |
| SYN                  | Pin_22          |
| ETB                  | Pin_23          |
| CAN                  | Pin_24          |
| EM                   | Pin_25          |
| SUB                  | Pin_26          |
| ESC                  | Pin_27          |
| FS                   | Pin_28          |
| GS                   | Pin_29          |
| '1'                  | Pin_Count       |
| '2'                  | Char_Count      |
| UC_A_Diaeresis       | A_Dots          |
| UC_A_Ring            | A_Ring          |
| UC_O_Diaeresis       | O_Dots          |
| UC_U_Diaeresis       | U_Dots          |

The following Enigma use defines pin<->character
translations:

| Enigma Use[^2] | Translation   |
| -------------- | ------------- |
| Pin_01         | 'A'           |
| Pin_02         | 'B'           |
| Pin_03         | 'C'           |
| Pin_04         | 'D'           |
| Pin_05         | 'E'           |
| Pin_06         | 'F'           |
| Pin_07         | 'G'           |
| Pin_08         | 'H'           |
| Pin_09         | 'I'           |
| Pin_10         | 'J'           |
| Pin_11         | 'K'           |
| Pin_12         | 'L'           |
| Pin_13         | 'M'           |
| Pin_14         | 'N'           |
| Pin_15         | 'O'           |
| Pin_16         | 'P'           |
| Pin_17         | 'Q'           |
| Pin_18         | 'R'           |
| Pin_19         | 'S'           |
| Pin_20         | 'T'           |
| Pin_21         | 'U'           |
| Pin_22         | 'V'           |
| Pin_23         | Character[^3] |
| Pin_24         | Character[^3] |
| Pin_25         | Character[^3] |
| Pin_26         | Character[^3] |
| Pin_27         | Character[^3] |
| Pin_28         | Character[^3] |
| Pin_29         | Character[^3] |
|                |               |
| Pin_Count      | Positive      |
| Char_Count     | Positive      |
|                |               |
| 'A'            | Pin_01        |
| 'B'            | Pin_02        |
| 'C'            | Pin_03        |
| 'D'            | Pin_04        |
| 'E'            | Pin_05        |
| 'F'            | Pin_06        |
| 'G'            | Pin_07        |
| 'H'            | Pin_08        |
| 'I'            | Pin_09        |
| 'J'            | Pin_10        |
| 'K'            | Pin_11        |
| 'L'            | Pin_12        |
| 'M'            | Pin_13        |
| 'N'            | Pin_14        |
| 'O'            | Pin_15        |
| 'P'            | Pin_16        |
| 'Q'            | Pin_17        |
| 'R'            | Pin_18        |
| 'S'            | Pin_19        |
| 'T'            | Pin_20        |
| 'U'            | Pin_21        |
| 'V'            | Pin_23        |
| 'W'            | Pin_NN[^3]    |
| 'X'            | Pin_NN[^3]    |
| 'Y'            | Pin_NN[^3]    |
| 'Z'            | Pin_NN[^3]    |
| A_Dots         | Pin_NN[^3]    |
| A_Ring         | Pin_NN[^3]    |
| O_Dots         | Pin_NN[^3]    |
| U_Dots         | Pin_NN[^3]    |

[^1]: UC_* = UPPERCASE. Lowercase letters are considered invalid.

[^2]: Pins 01-28 reserved for pins on rotors.

[^3]: Defines based on specific Enigma machine

## Standard Rotor Table

"Standard Rotor" is defined as used in a "Standard"
Enigma machine.
Letters used are A .. Z (26 pins).

| Enigma Use     | Translation |
| -------------- | ----------- |
| Pin_01         | 'A'         |
| Pin_02         | 'B'         |
| Pin_03         | 'C'         |
| Pin_04         | 'D'         |
| Pin_05         | 'E'         |
| Pin_06         | 'F'         |
| Pin_07         | 'G'         |
| Pin_08         | 'H'         |
| Pin_09         | 'I'         |
| Pin_10         | 'J'         |
| Pin_11         | 'K'         |
| Pin_12         | 'L'         |
| Pin_13         | 'M'         |
| Pin_14         | 'N'         |
| Pin_15         | 'O'         |
| Pin_16         | 'P'         |
| Pin_17         | 'Q'         |
| Pin_18         | 'R'         |
| Pin_19         | 'S'         |
| Pin_20         | 'T'         |
| Pin_21         | 'U'         |
| Pin_22         | 'V'         |
| Pin_23         | 'W'         |
| Pin_24         | 'X'         |
| Pin_25         | 'Y'         |
| Pin_26         | 'Z'         |
|                |             |
| Pin_Count      | Pin_26'Pos  |
| Char_Count     | Pin_26'Pos  |
|                |             |
| 'A'            | Pin_01      |
| 'B'            | Pin_02      |
| 'C'            | Pin_03      |
| 'D'            | Pin_04      |
| 'E'            | Pin_05      |
| 'F'            | Pin_06      |
| 'G'            | Pin_07      |
| 'H'            | Pin_08      |
| 'I'            | Pin_09      |
| 'J'            | Pin_10      |
| 'K'            | Pin_11      |
| 'L'            | Pin_12      |
| 'M'            | Pin_13      |
| 'N'            | Pin_14      |
| 'O'            | Pin_15      |
| 'P'            | Pin_16      |
| 'Q'            | Pin_17      |
| 'R'            | Pin_18      |
| 'S'            | Pin_19      |
| 'T'            | Pin_20      |
| 'U'            | Pin_21      |
| 'V'            | Pin_22      |
| 'W'            | Pin_23      |
| 'X'            | Pin_24      |
| 'Y'            | Pin_25      |
| 'Z'            | Pin_26      |

## Swedish B Rotor Table

The Swedish B machine is a modified Enigma B Type I and
can also be referred as the Enigma B Type II.

The main difference between the Swedish C and the
standard C is the letter 'W' is missing and the
letters A_Diaeresis, A_Ring, and O_Diaeresis are added.

The following translation table is used in the simulator:

| Enigma Use     | Translation |
| -------------- | ----------- |
| Pin_01         | 'A'         |
| Pin_02         | 'B'         |
| Pin_03         | 'C'         |
| Pin_04         | 'D'         |
| Pin_05         | 'E'         |
| Pin_06         | 'F'         |
| Pin_07         | 'G'         |
| Pin_08         | 'H'         |
| Pin_09         | 'I'         |
| Pin_10         | 'J'         |
| Pin_11         | 'K'         |
| Pin_12         | 'L'         |
| Pin_13         | 'M'         |
| Pin_14         | 'N'         |
| Pin_15         | 'O'         |
| Pin_16         | 'P'         |
| Pin_17         | 'Q'         |
| Pin_18         | 'R'         |
| Pin_19         | 'S'         |
| Pin_20         | 'T'         |
| Pin_21         | 'U'         |
| Pin_22         | 'V'         |
| Pin_23         | 'X'         |
| Pin_24         | 'Y'         |
| Pin_25         | 'Z'         |
| Pin_26         | A_Ring      |
| Pin_27         | A_Diaeresis |
| Pin_28         | O_Diaeresis |
|                |             |
| Pin_Count      | Pin_28'Pos  |
| Char_Count     | Pin_28'Pos  |
|                |             |
| 'A'            | Pin_01      |
| 'B'            | Pin_02      |
| 'C'            | Pin_03      |
| 'D'            | Pin_04      |
| 'E'            | Pin_05      |
| 'F'            | Pin_06      |
| 'G'            | Pin_07      |
| 'H'            | Pin_08      |
| 'I'            | Pin_09      |
| 'J'            | Pin_10      |
| 'K'            | Pin_11      |
| 'L'            | Pin_12      |
| 'M'            | Pin_13      |
| 'N'            | Pin_14      |
| 'O'            | Pin_15      |
| 'P'            | Pin_16      |
| 'Q'            | Pin_17      |
| 'R'            | Pin_18      |
| 'S'            | Pin_19      |
| 'T'            | Pin_20      |
| 'U'            | Pin_21      |
| 'V'            | Pin_22      |
| 'X'            | Pin_23      |
| 'Y'            | Pin_24      |
| 'Z'            | Pin_25      |
| A_Ring         | Pin_26      |
| A_Diaeresis    | Pin_27      |
| O_Diaeresis    | Pin_28      |

## Swedish Funkschl&#252;ssel C Rotor Table

Thought to be similar to the Swedish Enigma B, the
wiring of the rotors is not known.

The main difference between the Enigma C and the
Funkschl&#252;ssel C is the full alphabet A .. Z
is used along with A_Diaeresis, O_Diaeresis, and
U_Diaeresis. the A_Ring character is missing.
O_Diaeresis is missing from rotors, but is part of
keyboard.

Of note is what is displayed on the ring and
does not reflect the actual letters mapped to pins;
letters/numbers on the ring are only used for initial
setting of rotor starting positions.

The following translation table is used in the simulator:

| Enigma Use     | Translation   |
| -------------- | ------------- |
| Pin_01         | 'A'           |
| Pin_02         | A_Diaeresis   |
| Pin_03         | 'B'           |
| Pin_04         | 'C'           |
| Pin_05         | 'D'           |
| Pin_06         | 'E'           |
| Pin_07         | 'F'           |
| Pin_08         | 'G'           |
| Pin_09         | 'H'           |
| Pin_10         | 'I'           |
| Pin_11         | 'J'           |
| Pin_12         | 'K'           |
| Pin_13         | 'L'           |
| Pin_14         | 'M'           |
| Pin_15         | 'N'           |
| Pin_16         | 'O'           |
| Pin_17         | O_Diaeresis   |
| Pin_18         | 'P'           |
| Pin_19         | 'Q'           |
| Pin_20         | 'R'           |
| Pin_21         | 'S'           |
| Pin_22         | 'T'           |
| Pin_23         | 'U'           |
| Pin_24         | U_Diaeresis   |
| Pin_25         | 'V'           |
| Pin_26         | 'W'           |
| Pin_27         | 'Y'           |
| Pin_28         | 'Z'           |
| Pin_29         | 'X'[^4]       |
|                |               |
| Pin_Count      | Pin_28'Pos    |
| Char_Count     | Pin_29'Pos    |
|                |               |
| 'A'            | Pin_01        |
| 'B'            | Pin_03        |
| 'C'            | Pin_04        |
| 'D'            | Pin_05        |
| 'E'            | Pin_06        |
| 'F'            | Pin_07        |
| 'G'            | Pin_08        |
| 'H'            | Pin_09        |
| 'I'            | Pin_10        |
| 'J'            | Pin_11        |
| 'K'            | Pin_12        |
| 'L'            | Pin_13        |
| 'M'            | Pin_14        |
| 'N'            | Pin_15        |
| 'O'            | Pin_16        |
| 'P'            | Pin_18        |
| 'Q'            | Pin_19        |
| 'R'            | Pin_20        |
| 'S'            | Pin_21        |
| 'T'            | Pin_22        |
| 'U'            | Pin_23        |
| 'V'            | Pin_25        |
| 'W'            | Pin_26        |
| 'X'            | Pin_29[^4]    |
| 'Y'            | Pin_27        |
| 'Z'            | Pin_28        |
| A_Diaeresis    | Pin_02        |
| O_Diaeresis    | Pin_17        |
| U_Diaeresis    | Pin_24        |

[^4]: Letter 'X' is listed as Pin_29 for compatibility with the Enigma
simulator. It does not go through the encryption process since
the wiring has 'X' hardwired to the output.
