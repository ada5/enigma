
# Programming Notes

Sections of encryption path. Keyboard and lamps/printers are not part of
the emulator; listed for completeness only.

## Signal Path

Forward direction is from keyboard to UKW.

Reflected is from UKW to keyboard.

| Section | Direction |
| :------ | :-------- |
| Keyboard      | |
| Stecker       | Forward |
| ETW           | Forward |
| Rotor(s)      | Forward |
| UKW           | Forward to reflect |
| Rotor(s)      | Reflect |
| ETW           | Reflect |
| Stecker       | Reflect |
| Lamps/printer | |

Funtion of each section

| Section  | Function |
| :------- | :------- |
| Keyboard | Convert letter to electrical signal (letter to pin number) |
| Lamp     | Convert electrical signal to letter (pin number to letter) |
| Stecker [^2] | (Optional) Patch panel for convenient signal path swapping |
| ETW [^1] | Convert electrical connection from chassis wiring to rotor wiring |
| Rotor    | Swap input signal to mapped output |
| UKW [^1] | Reverse (reflect) signal path |

[1]: May also remap input similar to rotor.

[2]: Unless otherwise noted, will only map pairs; i.e. "AB" will map A->B (forward) and B->A (reflect).

## Attributes

| Section    | Map<br />Forward | Map<br />Reflect | Position | Offset [^3] |
| :-----:    | :-:              | :-:              | :-:      | :-:    |
| Stecker    | XXX              | XXX              |          |        |
| ETW [^5]   | XXX              | XXX              |          |        |
| Rotor [^5] | XXX              | XXX              | XXX      | XXX    |
| UKW  [^5]  | XXX              |                  | XXX      |        |
| Ring [^4]  |                  |                  |          | XXX    |

[3]: Rotor is position relative to chassis pin 1. Ring is relative to rotor pin 1.

[4]: Display is engraved on outside of ring. Display format is either letters or numbers.

[5]: Some models have field-changeable internal wiring (mapping).
