# Definitions

| Definition                   | Meaning                                        |
| ---------------------------- | ---------------------------------------------- |
| &#945;                       | Greek letter small "Alpha" |
| &#946;                       | Greek letter small "Beta" |
| &#947;                       | Greek letter small "Gamma" |
| &#948;                       | Greek letter small "Delta" |
| Abwehr                       | German Secret Service |
| Antriebtaste                 | Manual key to advance first rotor 1 position |
| Inner Key                    | Initial setting (rotors used, etc.) |
| Eintrittzwalze               | ETW |
| ETW                          | Entry disk (where signal enters first rotor) |
| Heer                         | German Army |
| Kabelpr&#252;fung            | Cable test (some models) |
| Kriegsmarine                 | German Navy |
| Leseger&#228;t               | Read-out device - typically a remote lamp panel |
| Lf                           | L&#252;ckenf&#252;llerwalze |
| L&#252;ckenf&#252;llerwalze  | Selectable gap wheel - user-selectable notches on rotor |
| Luftwaffe                    | German Air Force |
| Notch                        | Physical position on rotor for advancing next rotor |
| Outer Key                    | Initial wheel position based on message setting |
| Reichswehr                   | Early name of Wehrmacht |
| Ringstellung                 | Ring setting on the rotor(s) |
| Sicherheitsdienst            | Nazi SS intelligence agency |
| Stecker                      | Plug used with Steckerbrett |
| Steckerbrett                 | Plug board |
| Step/Stepping                | Act of moving rotor one position |
| Turnover                     | Letter/Number showing in window indicating where notch is located |
| UKW                          | Reflector - Last rotor that reflects signal back through machine |
| UKW-D                        | Field-changeable UKW |
| UMkehrwalze                  | UKW |
| Walzenlage                   | Rotor wheel order |
| Wehrmacht                    | Unified armed forces of Nazi Germany from 1935 to 1945 |
