
## Enigma Types

Initially, the Enigma was a 2-rotor design with fixed (non-swappable)
rotors. The first model was known as Enigma A. Although several variations
were proposed and modified, the minimum standard Enigma machines
were made with 3 rotors.

Most models use a standard 3-rotor encryption scheme using normal rotors.
The Kriegsmarine (Navy) M4 variant was issued with 4 wheels. Although the
typical Enigma could only use 3 rotors at once, the majority of the
Enigma machines were 3-rotor variants with 5 selectable rotors.

The M4 variant had 4 wheels and 2 styles. For left-middle-right positions,
the standard rotor type was used. Between the left rotor and the UKW, a 4th
rotor was used, but due to space requirements and mechanical needs, the 4th
rotor and the UKW were thinner in size to accomodate the extra rotor.


## Enigma Models

Below are the Enigma models. There were 2 basic types of Enigma
machines:

* Printing
* Glowlamp (Gl&#252;hlampenchiffriermaschine)

In addition, there are 2 types of stepping mechanisms:

* Pawls
* Cogwheels

Most of the Enigma machines were pawl style.

Printing machines were:

* Handel (Die Handelsmachine 1923)
* Schreib (Die Schreibende 1924-1926)
* H (H29 1929)

Glowlamp machines were:

* A (Die Kleine Militärmaschine 1924)
* B (1924-1925)
* C (1925)
* D (Commercial A26 1926)
* K (Commercial A27 1927)
* Z&#228;hlwerk (A28 1928)
* G (Z&#228;hlwerk G31 1931)
* I (Roman '1' 1932)
* M3 (M1, M2, M3 1932)
* Swiss-K (1939)
* Railway (1940)
* M4 (Kriegsmarine 1942)
* T (Tirpitz (Japanese) 1942)

### Signal Path

Genrally, the signal path for all Enigma machines follow this sequence:

* Step rotor(s)
* Keyboard (Convert letter input to signal input)
* Plugboard (optional)
* ETW (Convert chassis wiring to rotor pins)
* Rotor A
* Rotor B
* Rotor N (varies with model)
* UKW (Reflector)
* Rotor N reflect
* Rotor B reflect
* Rotor A reflect
* ETW reflect (Convert rotor pins to chassis wiring)
* Plugboard reflect (optional)
* Lamp/Print (Convert signal output to letter output)

### Enigma A (1924)

[The Crypto Museum: Enigma A](https://www.cryptomuseum.com/crypto/enigma/a/index.htm "The Crypto Museum")

Introduced in 1924. First glow lamp Enigma. Prototype shown publicly in August 1924 at the Universal Postal Union Congress in Stockholm, Sweden. Available in stock November 1924.

Emulation based on descriptions found in the archives
of FRA (Forsvarets Radion Anstalt - Swedish National Defence Radio Establisment).

#### Sections

* Keyboard
* ETW
* Rotor 1
* Rotor 2
* UKW
* Lamps

#### Options

1. Settable UKW (Not available in August 1924 prototype)
1. UKW-D (field rewireable)

#### Specifications

* Manual rotor advance using Antriebstaste key
* 26 positions (keyboard, lamps, rotors)
* 2-row keyboard
* Writable key labels
* Writable lamp labels
* Rotors labelled with numbers 00 .. 26
* UKW
* Notch locations on rotors not rings

### Enigma B Mark I (Standard)

[The Crypto Museum: Enigma B Mark I](https://www.cryptomuseum.com/crypto/enigma/b/index.htm#mk1 "The Crypto Museum")

### Enigma B Mark II (Swedish)

[The Crypto Museum: Enigma B Mark II](https://www.cryptomuseum.com/crypto/enigma/b/index.htm#mk2 "The Crypto Museum")

Special edition of Enigma B used by the Swedish SGS.

### Enigma C (Standard)

[The Crypto Museum: Enigma C (Standard)](https://www.cryptomuseum.com/crypto/enigma/c/index.htm#std "The Crypto Museum")

### Enigma C (Funkschl&#252;ssel)

[The Crypto Museum: Enigma C (Funkschl&#252;ssel)](https://www.cryptomuseum.com/crypto/enigma/c/index.htm#funk_c "The Crypto Museum")

Special edition of Enigma C used by the Swedish SGS.

#### Specifications

* UKW can be fitted in four different orientations signified by
&#945;, &#946;, &#947;, and &#948;.
* Rotor I labelled A, &#196;, B, .. U, &#220;, V .. Z
* Rotor II labelled 01, .. 28
* Rotor III labelled 30, .. 57
* Rotor IV labelled 60, .. 87
* Rotor V labelled AA, A&#196;, AB, .. AU, A&#220;, AV, .. AZ

Notes:

1. Rotor labels indicated for first 50 machines only.
Later machines used same label as Rotor I.
2. Letters X and AX are on rotor rings, but not &#213; or A&#213;.

### Enigma D

### Enigma D model A28 (Z&#228;hlwerk)

* First model with cogwheels instead of pawls for stepping (no double-step)

### Enigma G

#### Options

* Ch15a - Standard version
* Ch15b - Printer socket
* Ch15c - Plug board

#### Specifications

* Cogwheels instead of pawls for stepping (no double-step)

### Enigma K

### Enigma I (Roman '1')

### Enigma M3

### Enigma M4

### Enigma Swiss K

### Enigma Z

10-key number only version of Enigma. Noted for reference only.

### NEMA (Swiss)

[The Crypto Museum: NEMA](https://www.cryptomuseum.com/crypto/nema/index.htm "The Crypto Museum")

Swiss version of the Enigma K known as Neue Machine.

#### Sections

* Keyboard
* ETW
* Wheel 4
* Wheel 3
* Wheel 2
* Wheel 1
* UKW
* Lamps

#### Options

* Training Machine
* War Machine
* Foreign Office Machine

#### Specifications

* Notches are replaced by stepping wheels
* Wheels (rotors) are paired cipher wheel (C) and stepping wheel (S)
* Stepping configuration adjusted by 4 set screws for 6 different stepping configurations
* Stepping occurs backwards rather than forward (i.e., Z->Y->X ..)
