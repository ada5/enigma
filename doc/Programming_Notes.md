
# Programming Notes


* Ada.Characters.Latin_1 characters will be basis of characters
* Translation_Table from CSets.Translation_Table basis for M_Table
* A subtype of Translation_Table will be used for Enigma character/pin encryption table

## Structure

Basic structure of the Enigma enumlator


    Enigma
    +----------------------------------------------------------------------------+
    |                                                                            |
    | type Model_List := (...)                                                   |
    | Model : Model_List                                                         |
    | type Machine is null record                                                |
    |                                                                            |
    | function  Get_Machine  (Model) return Machine                              |
    | function  Encrypt      (Machine, Character) return Character               |
    | function  Encrypt      (Machine, String) return String                     |
    | function  Get_Display  (Machine) return String                             |
    | procedure Set_Rotor    (Machine, Rotor, .. )                               |
    |                                                                            |
    | Machine                                                                    |
    | +------------------------------------------------------------------------+ |
    | |                                                                        | |
    | | type Rotor_Array  : array (<>) of Rotor'Class                          | |
    | |                                                                        | |
    | | Model    : Model_List                                                  | |
    | | M_Table  : Translation_Table                                           | |
    | | Plug     : Rotor'Class        (Plugboard                               | |
    | | EKW      : Rotor'Class        (Chassis to rotor pin                    | |
    | | R1 .. RN : Rotor'Class        (Rotors                                  | |
    | | UKW      : Rotor'Class        (Reflector                               | |
    | | Rotors   : Rotor_Array                                                 | |
    | |                                                                        | |
    | | Rotor'Class                                                            | |
    | | +--------------------------------------------------------------------+ | |
    | | |                                                                    | | |
    | | | type Offset is mod 26                                              | | |
    | | | type Rotor_Flags is array () of boolean                            | | |
    | | | type Rotor_Pin   is array () of positive range 1 .. Pins'Last      | | |
    | | | type Rotor_Bool  is array () of boolean range  1 .. Pins'Last      | | |
    | | |                                                                    | | |
    | | | Flags      : Rotor_Flags                                           | | |
    | | | Ring       : Mod Pins'Last (offset from rotor pin 1)               | | |
    | | | Position   : Mod Pins'Last (offset from machine pin 1)             | | |
    | | | Map_F      : Rotor_Pin     (EKW to UKW direction)                  | | |
    | | | Map_R      : Rotor_Pin     (UKW to EKW direction)                  | | |
    | | | Map_N      : Rotor_Bool    (Step notch(es) for rotor)              | | |
    | | |                                                                    | | |
    | | +--------------------------------------------------------------------+ | |
    | +------------------------------------------------------------------------+ |
    +----------------------------------------------------------------------------+


* M_Table : See Character_Map for how Translation_Table is mapped for rotors

* R_Array : Array of rotors - (Plug, EKW, R1, .. , RN, UKW)

Rotor_Flags

| Flag    | Description |
| :---    | :---------- |
| Fixed   | Rotor is built-in to machine - cannot be changed |
| Set     | Rotor position (offset from machine pin 1) can be set |
| Step    | May step during encryption to next position |
| Change  | Input->Output mapping can be changed |
| Pairs   | Indicates if changable mapping must be in pairs |
| Encrypt | If rotor can encrypt input |
| Reflect | Redirects signal back through (reverse signal direction) |
| Ring    | Indicates if ring can change relative to rotor pin 1 |
| Notch   | Indicates if notch is part of ring or part of rotor |

* Encrypt is used where input signal is redirected to a different output signal
* Reflect specifies encryption is front-to-front (output is same side as input) rather
than front-to-back (output is opposite side as input)
* Set and Step
    * Some rotors can be set but not step (EKW, UKW, Plugboard)
    * Some rotors can step but not be set (preset R*)
    * Some rotors can do both (R*, UKW)


Rotor'Class

| Object   | Class    | Notes |
| :-----   | :----    | :---- |
| Flags    | Base     | Options that change between types of rotors |
| Ring     | Set/Step | Ring offset from pin 1 of rotor |
| Position | Set/Step | Rotor offset from pin 1 of machine |
| Map_F    | Base     | Maps front pin to back pin |
| Map_R    | Base     | Maps back pin to front pin |
| Map_N    | Step     | Maps notch/gear for next rotor stepping |

* Map_R : Some types do not have reflected mapping (UKW)
