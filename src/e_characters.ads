-------------------------------------------------------------------------------
--  characters.ads                                                           --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
-------------------------------------------------------------------------------

--  List of valid characters used in Enigma
--
--  Valid characters are a subset of Ada.Characters.Latin_1.
--  The main variance is upper case only, as well as the 4 extra
--  characters used in Swiss/Swedish language
--
--  To simplify conversions, we use the Character type to classify
--  both character/pin mappings as well as valid characters.

package E_Characters is
    pragma Pure;

    --  Use NUL to indicate invalid position
    NUL             : constant Character := Character'Val (00);

    -----------------------
    --  Pin assignments  --
    -----------------------

    --  Enigma rotors consist of either 26 or 28 pins.
    --
    --  There is one specific machine that has 29 keys on the
    --  keyboard, but the rotors only contain 28 pins.
    --
    --  We define 29 pins to include the extra character on
    --  the keyboard.
    P01             : constant Character := Character'Val (01);
    P02             : constant Character := Character'Val (02);
    P03             : constant Character := Character'Val (03);
    P04             : constant Character := Character'Val (04);
    P05             : constant Character := Character'Val (05);
    P06             : constant Character := Character'Val (06);
    P07             : constant Character := Character'Val (07);
    P08             : constant Character := Character'Val (08);
    P09             : constant Character := Character'Val (09);
    P10             : constant Character := Character'Val (10);
    P11             : constant Character := Character'Val (11);
    P12             : constant Character := Character'Val (12);
    P13             : constant Character := Character'Val (13);
    P14             : constant Character := Character'Val (14);
    P15             : constant Character := Character'Val (15);
    P16             : constant Character := Character'Val (16);
    P17             : constant Character := Character'Val (17);
    P18             : constant Character := Character'Val (18);
    P19             : constant Character := Character'Val (19);
    P20             : constant Character := Character'Val (20);
    P21             : constant Character := Character'Val (21);
    P22             : constant Character := Character'Val (22);
    P23             : constant Character := Character'Val (23);
    P24             : constant Character := Character'Val (24);
    P25             : constant Character := Character'Val (25);
    P26             : constant Character := Character'Val (26);
    P27             : constant Character := Character'Val (27);
    P28             : constant Character := Character'Val (28);
    P29             : constant Character := Character'Val (29);

    --  P_Rotor holds the number of pins on a rotor
    --  P_Keys  holds the number of valid keys on the keyboard
    P_Rotor         : constant Character := Character'Val (30);
    P_Keys          : constant Character := Character'Val (31);

    --  Space is not a valid character in an Enigma machine, but may have a use.
    --  Space           : constant Character := ' ';  -- Character'Val (32)
    ------------------
    --  Characters  --
    ------------------

    --  Letters 'A' through 'Z' are at positions 65 through 90

    --  Lowercase characters are not part of Enigma, but used to map to uppercase
    LC_A                 : constant Character := 'a';  -- Character'Val (97)
    LC_B                 : constant Character := 'b';  -- Character'Val (98)
    LC_C                 : constant Character := 'c';  -- Character'Val (99)
    LC_D                 : constant Character := 'd';  -- Character'Val (100)
    LC_E                 : constant Character := 'e';  -- Character'Val (101)
    LC_F                 : constant Character := 'f';  -- Character'Val (102)
    LC_G                 : constant Character := 'g';  -- Character'Val (103)
    LC_H                 : constant Character := 'h';  -- Character'Val (104)
    LC_I                 : constant Character := 'i';  -- Character'Val (105)
    LC_J                 : constant Character := 'j';  -- Character'Val (106)
    LC_K                 : constant Character := 'k';  -- Character'Val (107)
    LC_L                 : constant Character := 'l';  -- Character'Val (108)
    LC_M                 : constant Character := 'm';  -- Character'Val (109)
    LC_N                 : constant Character := 'n';  -- Character'Val (110)
    LC_O                 : constant Character := 'o';  -- Character'Val (111)
    LC_P                 : constant Character := 'p';  -- Character'Val (112)
    LC_Q                 : constant Character := 'q';  -- Character'Val (113)
    LC_R                 : constant Character := 'r';  -- Character'Val (114)
    LC_S                 : constant Character := 's';  -- Character'Val (115)
    LC_T                 : constant Character := 't';  -- Character'Val (116)
    LC_U                 : constant Character := 'u';  -- Character'Val (117)
    LC_V                 : constant Character := 'v';  -- Character'Val (118)
    LC_W                 : constant Character := 'w';  -- Character'Val (119)
    LC_X                 : constant Character := 'x';  -- Character'Val (120)
    LC_Y                 : constant Character := 'y';  -- Character'Val (121)
    LC_Z                 : constant Character := 'z';  -- Character'Val (122)

    --  *_Dots actual name is *_Diaeresis in letter maps
    A_Dots  : constant Character := Character'Val (196);  --  'A' with 2 dots above it
    A_Ring  : constant Character := Character'Val (197);  --  'A' with a ring above it
    O_Dots  : constant Character := Character'Val (214);  --  'O' with 2 dots above it
    U_Dots  : constant Character := Character'Val (220);  --  'U' with 2 dots above it

end E_Characters;
