-------------------------------------------------------------------------------
--  enigma.ads                                                               --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
-------------------------------------------------------------------------------

--  Base package and subroutines for Enigma emulator

package Enigma is

    type Models_List is (A);

end Enigma;
