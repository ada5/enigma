-------------------------------------------------------------------------------
--  utils.adb                                                                --
--  Copyright (c) 2021 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Helper routines for checking test packages                               --
-------------------------------------------------------------------------------

with Ada.Text_IO;

with Enigma.Wiring;

package body Utils is

    use Ada;
    use Enigma;

    package I_IO is new Ada.Text_IO.Integer_IO (Integer);

    --  Local helper routines

    procedure P_Digit (Number : Integer := 0) is
        W : Integer := 2;
    begin
        if Number < 10 then
            W := 1;
            Text_IO.Put ("0");
        end if;
        I_IO.Put (Item => Number, Width => W);
    end P_Digit;

    procedure P_Spaces (Count : Integer := 4) is
        S : String (1 .. Count) := (others => ' ');
    begin
        if Count >= 1 then
            Text_IO.Put (S);
        end if;
    end P_Spaces;

    procedure P_String (S      : String;
                        Lead   : Integer := 0;
                        Header : String  := "";
                        NL     : Boolean := False) is
    begin
        P_Spaces (Lead);
        Text_IO.Put (Header & "'");
        Text_IO.Put (S);
        Text_IO.Put ("'");
        if NL then
            Text_IO.New_Line;
        end if;
    end P_String;

    --  Main helper routines

    procedure Print_Pin_Array (Pins   : Wiring.Pin_Array;
                               Lead   : Integer := 0;      --  Indent from prefious group
                               Header : String  := "") is
        Indent : Integer := Lead + Header'Length + 1;      --  Include space for first open parens
        Index  : Integer := 0;          --  Loop counter
    begin
        P_Spaces (Lead);
        Text_IO.Put (Header & "(");
        for Index in Pins'Range loop
            if Index > 1 and then Index mod 10 = 1 then
                Text_IO.New_Line;
                P_Spaces (Indent);
            end if;
            P_Digit (Number => Pins (Index));
            if Index < Pins'Last then
                Text_IO.Put (", ");
            end if;
        end loop;
        Text_IO.Put_Line (")");
    end Print_Pin_Array;

    procedure Print_Wire_Record (R      : Wiring.Wire_Record;
                                 Lead   : Integer := 0;
                                 Header : String := "") is
        Indent : Integer := Lead + 4;
    begin
        P_Spaces (Lead);
        Text_IO.Put_Line (Header);
        P_String (Lead => Indent, Header => "Key     : ", S => R.Key, NL => True);
        P_String (Lead => Indent, Header => "Notch   : ", S => R.Notch, NL => True);
        P_String (Lead => Indent, Header => "Map     : ", S => R.Map, NL => True);
        Print_Pin_Array (Lead => Indent, Pins => R.Pins_F, Header => "Pins_F  : ");
        P_String (Lead => Indent, Header => "Reflect : ", S => R.Reflect, NL => True);
        Print_Pin_Array (Lead => Indent, Pins => R.Pins_R, Header => "Pins_R  : ");
    end Print_Wire_Record;

end Utils;