-------------------------------------------------------------------------------
--  test_runner.adb                                                          --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Test subprograms for Enigma emulator                                     --
-------------------------------------------------------------------------------

with Ahven.Framework;
with Ahven.Text_Runner;

with Enigma;
--  with Test_Enigma_Wiring;

--  Tests to run
with Test_Enigma;

procedure Test_Runner is
    use Ahven.Framework;

    Suite : constant Test_Suite_Access := Create_Suite (Suite_Name => "Enigma Tests");

begin
    Add_Test (Suite => Suite.all,
              T     => new Test_Enigma.TestCase);
    --  Add_Test (Suite => Suite.all,
    --      T     => new Test_Enigma_Wiring.TestCase);

    Ahven.Text_Runner.Run (Suite => Suite);
    Release_Suite (T => Suite);

end Test_Runner;
