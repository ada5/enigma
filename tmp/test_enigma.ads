-------------------------------------------------------------------------------
--  test_enigma.ads                                                          --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Test routines for package Enigma subroutines                             --
-------------------------------------------------------------------------------

with Ahven.Framework;

package Test_Enigma is
    type TestCase is new Ahven.Framework.Test_Case with null record;

    procedure Initialize (T : in out TestCase);

    --  Tests for Enigma package
    --  procedure Test_Get_Map_Standard;
    --  procedure Test_Get_Map_Swedish;

end Test_Enigma;
