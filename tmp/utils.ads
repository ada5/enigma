-------------------------------------------------------------------------------
--  utils.ads                                                                --
--  Copyright (c) 2021 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Helper routines for checking test packages                               --
-------------------------------------------------------------------------------

with Ada.Text_IO;
with Enigma.Wiring;

package Utils is
    use Ada;
    use Enigma;

    --  Helper routines
    procedure P_Digit  (Number : Integer := 0);

    procedure P_Spaces (Count : Integer := 4);

    procedure P_String (S      : String;
                        Lead   : Integer := 0;
                        Header : String  := "";
                        NL     : Boolean := False);

    --  Main routines
    procedure Print_Pin_Array (Pins   : Wiring.Pin_Array;
                               Lead   : Integer := 0;
                               Header : String  := "");

    procedure Print_Wire_Record (R      : Wiring.Wire_Record;
                                 Lead   : Integer := 0;
                                 Header : String := "");

end Utils;