-------------------------------------------------------------------------------
--  enigma.characters.ads                                                    --
--  Copyright (c) 2021 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Base package for Enigma characters and strings                           --
-------------------------------------------------------------------------------

with CSets;
with Ada.Characters.Latin_1;

package Enigma.Characters is

    --  Base package for keeping track of Enigma character sets.
    --  Base characters are from the Latin-1 character set.
    --
    --  We use the Latin-1 character set since both the standard
    --  alphabet as well as 3 Swedish characters are in the base
    --  Latin-1 charater set as defined in the Ada manual.
    --
    --  The upside of this setup is being able to use wide_character
    --  and wide_wide_character built-in translations.
    --
    --  Standard characters are defined in Enigma.Characters.Standard
    --
    --  Swedish-28 characters are defined in Enigma.Characters.Swedish_28
    --      Standard characters minus the character 'X'.
    --      Swedish characters UC_A_Diaeresis, UC_A_Ring, and UC_U_Diaeresis
    --      are added.
    --
    --  Swedish-29 characters are defined in Enigma.Characters.Swedish_29
    --      Special-case for the Funkschlüssel C variant that uses the
    --      Swedish-28 character mapping for encryption, but includes
    --      the character 'X' that is directly wired rather than encyrpted
    --      through the rotors.

    --  CSets: type Translate_Table is array (Character) of Character;
    --      To use a common type for our letter -> pin conversion
    --      and pin -> pin encryption table, and make it easier to actually
    --      accomplish some of the tasks of encryption, we will abuse this
    --      format for our purposes in order to have a common base between
    --      the several different rotor types. Rotor types can have
    --      a pin count of 26 or 28 pins, where the enigma device itself
    --      can have a key count of 26, 28, or 29 keys.
    --      We also use this type for mapping lowercase letters to
    --      uppercase letters since Enigma machines only had uppercase
    --      keys.
    --      Rotor mapping rules are:
    --          Translate_Table'Pos (1 .. Pin_Count) maps input pin
    --          to output pin (forward)
    --
    --          Translate_Table'Pos (31 .. 31+Pin_Count) maps input pin
    --          to output pin (reflected)
    --
    --          Translate_Table'Pos (<first UC_letter> .. <last UC_letter>) maps
    --          uppercase letter to pin
    --
    --          Translate_Table'Pos (101 .. 101+Pin_Count) maps
    --          pin to uppercase letter
    --
    --      Message mapping rules are:
    --          Translate_Table'Pos (1 .. Pin_Count) maps input pin
    --          to uppercase letter.
    --
    --          Translate_Table'Pos (<first UC_letter> .. <last UC_letter>) maps
    --          letter to pin.
    --
    --          Translate_Table'Pos (<first LC_letter> .. <last LC_letter>) maps
    --          letter to pin.
    --
    --      Funkschlüssel C variation:
    --          Pin/Letter mapping is the same as Swedish-28 with exception of
    --          character 'X'.
    --          Pins assignments and letter mapping are set to the same as Swedish-28.
    --          Range is set to the same as Swedish-28.
    --          Character 'X' is mapped to Pin_Count + 1 range will work as normal with
    --          rotor pins.
    --
    --  CSets: type Char_Array_Flags is array (Character) of Boolean;
    --      This is the base type for keeping track of valid characters
    --      for Enigma and valid pins for the rotors.

    package L renames Enigma.Characters.Latin_1;

end Enigma.Characters;
