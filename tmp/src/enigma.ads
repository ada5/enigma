-------------------------------------------------------------------------------
--  enigma.ads                                                               --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Base package and subroutines for Enigma emulator                         --
-------------------------------------------------------------------------------

--  WARNING: Ensure you are using Latin-1 (ISO-8859-1, Western CP-1252)
--           character set in your editor,
--           otherwise you may end up with wide-characters for the 3
--           Swedish characters that are not ASCII characters.
--           If any of the 3 characters end up being wide characters,
--           "Constraint Error" will be raised.

package Enigma is

    Wire_Error : exception;  --  Wire map errors

    type Keys is (Standard, Swedish);

    S_Length     : constant Integer := 28;  --  Maximum length to allow for Swedish_Map

    subtype E_String is String (1 .. S_Length);

    --  Remember to keep string length = S_Length
    Standard_Map : constant E_String := "ABCDEFGHIJKLMNOPQRSTUVWXYZ  ";
    --  Ahven has a problem with Unicode (multibyte) characters
    --  Use Latin-1 (MS Windows cp1252) codepage so they stay single-byte characters
    Swedish_Map  : constant E_String := "ABCDEFGHIJKLMNOPQRSTUVXYZ���";

    --  Helper function into Ada.Strings.Fixed.Index
    function Get_Index (C : Character;
                        K : E_String := Standard_Map)
        return Integer;

    function Set_E_String (S : String)
        return E_String;

end Enigma;
