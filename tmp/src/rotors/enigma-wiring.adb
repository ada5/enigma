-------------------------------------------------------------------------------
--  enigma-wiring.ads                                                        --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Map of known Enigma machine wiring maps                                  --
-------------------------------------------------------------------------------

with Ada.Strings.Fixed; use Ada.Strings;
with Ada.Text_IO; use Ada.Text_IO;

package body Enigma.Wiring is

    --  Define here since Wiring_Options needs this
    function Get_Notch (N   : String;
                        Key : E_String := Standard_Map)
        return E_String is
        Count  : Integer   := 0;     -- Loop counter
        Idx    : Integer   := 0;
        Ret    : E_String  := (others => ' ');
        SCheck : String    := " ";
        LCheck : String    := Fixed.Trim (Source => String (Key),
                                          Side => Right);
    begin
        for Count in N'Range loop
            if N (Count) /= ' ' then
                Idx := Get_Index (C => N (Count), K => Key);
                if Idx /= 0 then
                    Ret (Idx) := N (Count);
                else
                    raise Wire_Error with ("Invalid character '" & N (Count) & "' in Notch");
                end if;
            end if;
        end loop;
        return Ret;
    end Get_Notch;

    Wiring_Options : constant Wiring_Options_Array := (
            (Key   => Standard_Map,
             Map   => Set_E_String ("EKMFLGDQVZNTOWYHXUSPAIBRCJ"),    --  Option 01   Step 'Q'
             Notch => Set_E_String ("Q")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("AJDKSIRUXBLHWTMCQGZNPYFVOE"),    --  Option 02   Step 'E'
             Notch => Set_E_String ("E")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("BDFHJLCPRTXVZNYEIWGAKMUSQO"),    --  Option 03   Step 'V'
             Notch => Set_E_String ("V")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("ESOVPZJAYQUIRHXLNFTGKDCMWB"),    --  Option 04   Step 'J'
             Notch => Set_E_String ("J")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("VZBRGITYUPSDNHLXAWMJQOFECK"),    --  Option 05   Step 'Z'
             Notch => Set_E_String ("Z")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("EJMZALYXVBWFCRQUONTSPIKHGD"),    --  Option 06
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("YRUHQSLDPXNGOKMIEBFZCWVJAT"),    --  Option 07
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("FVPJIAOYEDRZXWGCTKUQSBNMHL"),    --  Option 08
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("JPGVOUMFYQBENHZRDKASXLICTW"),    --  Option 09   Step 'ZM'
             Notch => Set_E_String ("ZM")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("NZJHGRCXMYSWBOUFAIVLPEKQDT"),    --  Option 10   Step 'ZM'
             Notch => Set_E_String ("ZM")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("FKQHTLXOCBJSPDZRAMEWNIUYGV"),    --  Option 11   Step 'ZM'
             Notch => Set_E_String ("ZM")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("LEYJVCNIXWPBQMDRTAKZGFUHOS"),    --  Option 12
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("FSOKANUERHMBTIYCWLQPZXVGJD"),    --  Option 13
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("ENKQAUYWJICOPBLMDXZVFTHRGS"),    --  Option 14
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("RDOBJNTKVEHMLFCWZAXGYIPSUQ"),    --  Option 15
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("VEOSIRZUJDQCKGWYPNXAFLTHMB"),    --  Option 16   Step 'Q'
             Notch => Set_E_String ("Q")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("UEMOATQLSHPKCYFWJZBGVXINDR"),    --  Option 17   Step 'E'
             Notch => Set_E_String ("E")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("TZHXMBSIPNURJFDKEQVCWGLAOY"),    --  Option 18   Step 'V'
             Notch => Set_E_String ("V")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("WTOKASUYVRBXJHQCPZEFMDINLG"),    --  Option 19   Step 'Q'
             Notch => Set_E_String ("Q")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("GJLPUBSWEMCTQVHXAOFZDRKYNI"),    --  Option 20   Step 'E'
             Notch => Set_E_String ("E")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("JWFMHNBPUSDYTIXVZGRQLAOEKC"),    --  Option 21   Step 'V'
             Notch => Set_E_String ("V")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("HEJXQOTZBVFDASCILWPGYNMURK"),    --  Option 22   Step 'Z'
             Notch => Set_E_String ("Z")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("MOWJYPUXNDSRAIBFVLKZGQCHET"),    --  Option 23
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("CIAGSNDRBYTPZFULVHEKOQXWJM"),    --  Option 24
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("LPGSZMHAEOQKVXRFYBUTNICJDW"),    --  Option 25   Step 'SUVWZABCEFGIKLOPQ'
             Notch => Set_E_String ("SUVWZABCEFGIKLOPQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("SLVGBTFXJQOHEWIRZYAMKPCNDU"),    --  Option 26   Step 'STVYZACDFGHKMNQ'
             Notch => Set_E_String ("STVYZACDFGHKMNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("CJGDPSHKTURAWZXFMYNQOBVLIE"),    --  Option 27   Step 'UWXAEFHKMNR'
             Notch => Set_E_String ("UWXAEFHKMNR")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("IMETCGFRAYSQBZXWLHKDVUPOJN"),    --  Option 28
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("QWERTZUIOASDFGHJKPYXCVBNML"),    --  Option 29
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("DMTWSILRUYQNKFEJCAZBPGXOHV"),    --  Option 30   Step 'SUVWZABCEFGIKLOPQ'
             Notch => Set_E_String ("SUVWZABCEFGIKLOPQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("HQZGPJTMOBLNCIFDYAWVEUSRKX"),    --  Option 31   Step 'STVYZACDFGHKMNQ'
             Notch => Set_E_String ("STVYZACDFGHKMNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("UQNTLSZFMREHDPXKIBVYGJCWOA"),    --  Option 32   Step 'UWXAEFHKMNR'
             Notch => Set_E_String ("UWXAEFHKMNR")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("RULQMZJSYGOCETKWDAHNBXPVIF"),    --  Option 33
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("RCSPBLKQAUMHWYTIFZVGOJNEXD"),    --  Option 34   Step 'SUVWZABCEFGIKLOPQ'
             Notch => Set_E_String ("SUVWZABCEFGIKLOPQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("WCMIBVPJXAROSGNDLZKEYHUFQT"),    --  Option 35   Step 'STVYZACDFGHKMNQ'
             Notch => Set_E_String ("STVYZACDFGHKMNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("FVDHZELSQMAXOKYIWPGCBUJTNR"),    --  Option 36   Step 'UWXAEFHKMNR'
             Notch => Set_E_String ("UWXAEFHKMNR")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("WLRHBQUNDKJCZSEXOTMAGYFPVI"),    --  Option 37   Step 'SUVWZABCEFGIKLOPQ'
             Notch => Set_E_String ("SUVWZABCEFGIKLOPQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("TFJQAZWMHLCUIXRDYGOEVBNSKP"),    --  Option 38   Step 'STVYZACDFGHKMNQ'
             Notch => Set_E_String ("STVYZACDFGHKMNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("QTPIXWVDFRMUSLJOHCANEZKYBG"),    --  Option 39   Step 'SWZFHMQ'
             Notch => Set_E_String ("SWZFHMQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("LPGSZMHAEOQKVXRFYBUTNICJDW"),    --  Option 40   Step 'Y'
             Notch => Set_E_String ("Y")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("SLVGBTFXJQOHEWIRZYAMKPCNDU"),    --  Option 41   Step 'E'
             Notch => Set_E_String ("E")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("CJGDPSHKTURAWZXFMYNQOBVLIE"),    --  Option 42   Step 'N'
             Notch => Set_E_String ("N")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("PEZUOHXSCVFMTBGLRINQJWAYDK"),    --  Option 43   Step 'Y'
             Notch => Set_E_String ("Y")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("ZOUESYDKFWPCIQXHMVBLGNJRAT"),    --  Option 44   Step 'E'
             Notch => Set_E_String ("E")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("EHRVXGAOBQUSIMZFLYNWKTPDJC"),    --  Option 45   Step 'N'
             Notch => Set_E_String ("N")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("VEZIOJCXKYDUNTWAPLQGBHSFMR"),    --  Option 46   Step 'SUYAEHLNQ'
             Notch => Set_E_String ("SUYAEHLNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("HGRBSJZETDLVPMQYCXAOKINFUW"),    --  Option 47   Step 'SUYAEHLNQ'
             Notch => Set_E_String ("SUYAEHLNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("NWLHXGRBYOJSAZDVTPKFQMEUIC"),    --  Option 48   Step 'SUYAEHLNQ'
             Notch => Set_E_String ("SUYAEHLNQ")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("NSUOMKLIHZFGEADVXWBYCPRQTJ"),    --  Option 49
             Notch => Set_E_String ("")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("JGDQOXUSCAMIFRVTPNEWKBLZYH"),    --  Option 50   Step 'N'
             Notch => Set_E_String ("N")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("NTZPSFBOKMWRCJDIVLAEYUXHGQ"),    --  Option 51   Step 'E'
             Notch => Set_E_String ("E")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("JVIUBHTCDYAKEQZPOSGXNRMWFL"),    --  Option 52   Step 'Y'
             Notch => Set_E_String ("Y")),
            (Key   => Standard_Map,
             Map   => Set_E_String ("QYHOGNECVPUZTFDJAXWMKISRBL"),    --  Option 53
             Notch => Set_E_String ("")),
            -- ------------------------------------------------------------- --
            --                              Swedish maps                     --
            -- ------------------------------------------------------------- --

            --  For KDE Fedora, go to
            --  Settings -> Input -> Keyboard -> Advanced ->
            --  Position of Compose Key
            --
            --  Select which key is <Multi_Key> (I use left-Win)
            --
            --  "�" Swedish A with diaeresis
            --     <Multi_Key+Shift+double-quote>A
            --  "�" Swedish A with ring
            --     <Multi_Key+Shift>AA
            --  "�" Swedish O with diaeresis
            --     <Multi_Key+Shift+double-quote>O
            --
            (Key   => Swedish_Map,
             Map   => Set_E_String ("PSBG�XQJDHO�UCFRTEZV�INLYMKA"),  --  Option 54  Step '�'
             Notch => Set_E_String ("�")),
            (Key   => Swedish_Map,
             Map   => Set_E_String ("CHNSY�ADMOTRZXB�IG�EKQUPFLVJ"),  --  Option 55  Step '�'
             Notch => Set_E_String ("�")),
            (Key   => Swedish_Map,
             Map   => Set_E_String ("�VQIA�XRJB�ZSPCFYUNTHDOMEKGL"),  --  Option 56  Step '�'
             Notch => Set_E_String ("�")),
            (Key   => Swedish_Map,
             Map   => Set_E_String ("LDGB�NCPSKJAVFZHXUI�RMQ�OTEY"),  --  Option 57
             Notch => Set_E_String (""))
        );

    --  Build Wire_Record
    function Get_Wire_Map (Map : String;
                           Notch : String := "";
                           Key   : String := Standard_Map
                          )
        return Wire_Record is
        R   : Wire_Record;
        M   : String  := Fixed.Trim (Source => Map);    --  Trimmed Map
        N   : String  := Fixed.Trim (Source => Notch);  --  Trimmed Notch
        K   : String  := Fixed.Trim (Source => Key);    --  Trimmed Key
        --  PinF is index of input  (forward)   pin
        --  PinR is index of output (reflected) pin
        PinF, PinR   : Integer   := 0;
    begin
        R.Length := M'Length;
        R.Map    := M;
        R.Key    := K;

        for PinF in R.Map'Range loop
            PinR := Get_Index (C => R.Map (PinF), K => R.Key);
            --  Check for invalid character in map
            if PinR = 0 then
                raise Wire_Error with "Invalid character in map '" & R.Map (PinF) & "' at index " & PinF'Img;
            end if;
            --  Put input symbol in proper location of reflected string
            R.Reflect (PinR) := R.Key (PinF);
            --  Map the pins in the lookup arrays
            R.Pins_F (PinF)  := PinR;
            R.Pins_R (PinR)  := PinF;
        end loop;

        return R;
    end Get_Wire_Map;

    function Get_Wire_Map (Z     : String;
                           Map   : String;
                           Notch : String := "";
                           Key   : String := Standard_Map
                          )
        return Wire_Record is
        M   : String                := Fixed.Trim (Source => Map,   Side => Right);  --  Trimmed Map
        N   : String                := Fixed.Trim (Source => Notch, Side => Right);  --  Trimmed Notch
        K   : String                := Fixed.Trim (Source => Key,   Side => Right);  --  Trimmed Key
        WOR : Wiring_Options_Record := (Map   => Set_E_String (S => ""),
                                        Notch => Set_E_String (S => ""),
                                        Key   => Key
                                       );
        L   : Integer               := M'Length;
        I   : Integer               := 0;  --  Index of C in K
    begin
        --  Verify length of map and get correct key
        --  Map must be same length as either Standard_Map or Swedish_Map,
        --      otherwise it won't match the rotor pins
        if L = Fixed.Trim (Source => Standard_Map, Side => Right)'Length then
            null;
        elsif L = Fixed.Trim (Source => Swedish_Map, Side => Right)'Length then
            null;
        else
            raise Wire_Error with "Get_Wire_Map: Invalid map length " & L'Img;
        end if;

        --  Verify map letters and valid mapping
        for Idx in M'Range loop
            if M (Idx) = ' ' then
                raise Wire_Error with "Get_Wire_Map: Spaces not allowed";
            end if;
            I := Get_Index (C => M (Idx), K => WOR.Key);
            if I = 0 then
                raise Wire_Error
                with "Get_Wire_Map: Invalid character '" & M (Idx) & "'";
            elsif WOR.Map (I) /= ' ' then
                raise Wire_Error with "Get_Wire_Map: Duplicate character : " & M (Idx);
            end if;
            --  Add to map record
            WOR.Map (I) := M (Idx);
        end loop;
        WOR.Notch := Get_Notch (N => N, Key => WOR.Key);
        return Get_Wire_Map (Wiring => WOR);
    end Get_Wire_Map;

    --  Build Wire_Record from Wiring Wiring_Options_Array
    function Get_Wire_Map (Option : Positive)
        return Wire_Record is
        Opt : Wiring_Options_Record := Wiring_Options (Option);
    begin
        return Get_Wire_Map (Map   => Opt.Map,
                             Notch => Opt.Notch,
                             Key   => Opt.Key);
    end Get_Wire_Map;

    --  Return Wire_Options_Record from Wiring_Options_Array
    function Get_Wiring_Option (Option : Positive)
        return Wiring_Options_Record is
    begin
        return Wiring_Options (Option);
    end Get_Wiring_Option;

end Enigma.Wiring;
