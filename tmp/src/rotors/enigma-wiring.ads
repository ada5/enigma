-------------------------------------------------------------------------------
--  enigma-wiring.ads                                                        --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Map of known Enigma machine wiring maps                                  --
-------------------------------------------------------------------------------

--  Ahven has a problem with Swedish characters. Make sure to use Latin-1
--  (MS Windows cp1252) encoding so they are single-byte characters

package Enigma.Wiring is

    type Wiring_Options_Array is private;

    type Pin_Array is array (1 .. S_Length) of Positive;

    type Wire_Record is record
        Length  : Positive range 1 .. S_Length;    --  Length of trimmed Key E_String
        --  Key matches symbol to pin number by position.
        --  ex. Standard_Map (A .. Z) matches pin number input (1 .. 26)
        Key     : E_String  := (others => ' ');    --  Letter to index mapping
        --  Map matches input symbol to pin number output
        Map     : E_String  := (others => ' ');    --  Forward letter mapping
        --  Reflect matches the pin output back to the pin input
        --  Note - Reflect is used for debugging purposes
        Reflect : E_String  := (others => ' ');    --  Reflected letter mapping
        --  Notch, if symbol is present, specifies which symbol position indicates
        --  which pin indicates next rotor in sequence is toggled
        Notch   : E_String  := (others => ' ');    --  Letter to pin index for notch
        --  Pins_F (forward) and Pins_R (reflected) are lookup tables for Map (Pins_F)
        --  and Reflected (Pins_R) pin numbers
        Pins_F  : Pin_Array := (others => 99);     --  Map letter to forward rotor pin
        Pins_R  : Pin_Array := (others => 99);     --  Map letter to reflect rotor pin
    end record;

    --  This one is only used to keep track of the known wiring maps
    type Wiring_Options_Record is record
        Key     : E_String := Standard_Map;
        Map     : E_String := (others => ' ');
        Notch   : E_String := (others => ' ');
    end record;

    --  Returns an E_String where notch letters are set based on key used,
    --  all other positions are set to blank.
    --  ex: Notch => "Q", Key => Standard_Map
    --      Return   "                Q           "
    --  ex: Notch => "SUVWZABCEFGIKLOPQ", Key => Standard_Map
    --      Return   "ABC EFG I KL  OPQ S UVW  Z  "
    function Get_Notch (N   : String;
                        Key : E_String := Standard_Map)
        return E_String;

    --  Build Wire_Record from strings
    function Get_Wire_Map (Map   : String;
                           Notch : String := "";
                           Key   : String := Standard_Map
                          )
        return Wire_Record;

    --  Placeholder until I get the function moved to above
    function Get_Wire_Map (Z     : String;
                           Map   : String;
                           Notch : String := "";
                           Key   : String := Standard_Map
                          )
        return Wire_Record;

    --  Build Wire_Record using Wiring_Option index
    function Get_Wire_Map (Option : Positive)
        return Wire_Record;

    --  Helper function to retrieve pre-set wiring map
    function Get_Wiring_Option (Option : Positive)
        return Wiring_Options_Record;

private

    type Wiring_Options_Array is array (1 .. 57) of Wiring_Options_Record;

end Enigma.Wiring;
