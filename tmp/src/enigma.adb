-------------------------------------------------------------------------------
--  enigma.adb                                                               --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Base package and subroutines for Enigma emulator                         --
-------------------------------------------------------------------------------

with Ada.Strings.Fixed; use Ada.Strings;

package body Enigma is

    --  Helper function into Ada.Strings.Fixed.Index
    function Get_Index (C : Character;
                        K : E_String := Standard_Map)
        return Integer is
        LCheck : String    := Fixed.Trim (Source => String (K),
                                          Side   => Right);
        R : Integer := 0;
    begin
        for Idx in LCheck'Range loop
            if C = LCheck (Idx) then
                R := Idx;
                exit;
            end if;
        end loop;
        return R;
    end Get_Index;

    function Set_E_String (S : String)
        return E_String is
        R : E_String := (others => ' ');
    begin
        Fixed.Overwrite (Source => R, Position => 1, New_Item => S);
        return R;
    end Set_E_String;

end Enigma;
