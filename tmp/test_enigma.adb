-------------------------------------------------------------------------------
--  test_enigma.adb                                                          --
--  Copyright (c) 2020 Ken Roberts                                           --
--                                                                           --
--  See Copyright.txt in root directory.                                     --
--                                                                           --
--  Test routines for package Enigma subroutines                             --
-------------------------------------------------------------------------------

with Ahven.Framework;
with Ada.Text_IO;
with Ada.Characters.Handling;

with Enigma;

package body Test_Enigma is
    use Ahven;
    use Enigma;

    procedure Initialize (T : in out Testcase) is
    begin
        T.Set_Name (Name => "Test Enigma subroutines");
        --  T.Add_Test_Routine
        --      (Routine => Test_Get_Map_Standard'Access,
        --       Name    => "Check Get_Wiring_Option (Standard)");
    end Initialize;

    -------------------------------------------------------------------------------
    --                               Begin tests routines                        --
    -------------------------------------------------------------------------------

end Test_Enigma;
