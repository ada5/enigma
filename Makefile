NAME      = enigma
PREFIX   ?= build
TESTDIR   = tests
OBJDIR    = $(PREFIX)/obj/$(OS)
COVDIR    = $(OBJDIR)/coverage
HTMLDIR   = $(PREFIX)/coverage
LIBDIR    = $(PREFIX)/lib
SRCDIR    = src
GPR_FILES = gnat/*.gpr

MAJOR    = 0
MINOR    = 0
REVISION = 1
VERSION  = $(MAJOR).$(MINOR).$(REVISION)
ENIGMA   = lib$(NAME)-$(VERSION)
TARBALL  = $(ENIGMA).tar.bz2

SO_LIBRARY   = lib$(NAME).so.$(VERSION)
LIBRARY_KIND = dynamic

# Compile setup
# Currently only Linux supported
OS         = linux
NUM_CPUS  ?= 1

# GNAT_BUILDER_FLAGS may be overridden in the
# environment or on the command line.
GNAT_BUILDER_FLAGS ?= -R -j$(NUM_CPUS)
# GMAKE_OPTS should not be overridden because -p is essential.
GMAKE_OPTS = -p ${GNAT_BUILDER_FLAGS} \
    $(foreach v,ADAFLAGS LDFLAGS OS VERSION,'-X$(v)=$($(v))')

# GNU-style directory variables
prefix      = ${PREFIX}
exec_prefix = ${prefix}
includedir  = ${prefix}/include
libdir      = ${exec_prefix}/lib
gprdir      = ${prefix}/lib/gnat

.PHONY: tests clean

all: build_tests

clean:
	rm -rf $(PREFIX)
	# $(MAKE) -C doc clean

build_tests: clean
	gprbuild $(GMAKE_OPTS) enigma_tests.gpr -XLIBRARY_KIND=static -XBUILD=tests

tests: build_tests
	$(OBJDIR)/$(TESTDIR)/test_runner

coverage:
	rm -f $(COVDIR)/*.gcda
	gprbuild $(GMAKE_OPTS) enigma_tests.gpr -XLIBRARY_KIND=static -XBUILD=coverage
	$(COVDIR)/test_runner || true
	lcov -c -d $(COVDIR) -o $(COVDIR)/cov.info
	lcov -e $(COVDIR)/cov.info "$(PWD)/src/*.adb" -o $(COVDIR)/cov.info
	genhtml --no-branch-coverage $(COVDIR)/cov.info -o $(HTMLDIR)

style: clean
	gprbuild $(GMAKE_OPTS) -P enigma_tests.gpr -XLIBRARY_KIND=static -XBUILD=style

