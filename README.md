# Ada Enigma

![](doc/img/300px-Ada_Mascot_with_slogan.png "Ada Logo")
![](doc/img/enigma_logo-300x132.png "Enigma Logo")

An Ada version of the Enigma Encryption Machine
To help me learn the Ada programming language.

Ultimate goal will be an Enigma library that can be used by other
programs to create an Enigma machine backend that they can use
by command line or GUI usage for encryption/decryption of plain
text messages.

Documentation uses the Gitlab flavored version of Markdown.

Target compiler is Ada 2005. Reference Manual available at
[Ada Information Clearinghouse (ADAIC)](https://www.adaic.org/ada-resources/standards/ada05/ "Ada 2005 Reference at ADAIC").

All references to Ada programming notes will use the GCC version of Ada
known as [GNAT GCC](https://gcc.gnu.org/wiki/GNAT "GNAT GCC").

[[_TOC_]]

# Enigma Basics

For the full description of the Enigma machine, visit
[The Crypto Museum: Enigma](https://www.cryptomuseum.com/crypto/enigma/index.htm "The Crypto Museum")
for everything you want to know.

The documentation here only describes the basics for a local reference.

## Enigma Variations

During it's production cycle, there were 3 types of Enigma machines made:

1. Enigma machine with 26 encryption keys
    * 26 input keys with appropriate letters
    * 26 pins on the rotors for encryption
    * 26 lamps for displaying encrypted letter labelled same as input


2. Enigma machine with 28 encryption keys
    * 28 input keys with appropriate letters
    * 28 pins on the rotors for encryption
    * 28 lamps for displaying encrypted letter labelled same as input


3. Enigma machine with 28 encryption keys and 1 unencrypted key
    * 29 input keys with appropriate letters
    * 28 pins on the rotors for encrypting all except the 'X' character
    * 29 lamps for displaying encrypted letter labelled same as input

# Basic Usage

Projected usage:

```ada

    with Enigma;

    --  Enumerated list of Enigma models supported
    Enigma.Model_List

    --  Due to variations in models, Machine record is
    --  created based on model selected.
    type Enigma.Machine is null record

    --  Returns a colon-separated list of models available
    -- "A:C:Funkschlussel1:Swiss:..."
    S := Enigma.Get_Models ();

    --  Main emulator that packages instantiate
    --  Model is the base model of Enigma; ex. "C" (Enigma model C)
    M : Enigma.Get_Machine (Model => [Enigma.Model_List]);

    --  Return a colon-separated list of available preset rotors
    --  "I:II: .. :CUSTOM"
    S := Enigma.Get_Rotors ();

    --  Rotor options
    Enigma.Set_Rotor (M     => Machine,
                      Slot  => [number],       --  Put selected rotor into slot
                      Map   => [string],       --  Set rotor mapping
                      Notch => [string],       --  Set rotor stepping locations
                      Pos   => [character],    --  Set rotor to position
                      Ring  => [character]);   --  Set ring letter to rotor pin 1
    --
    --  Optional calls
    --
    Enigma.Set_Rotor (M     => Machine,
                      Slot  => [number],
                      Map   => [string],
                      Notch => [string]);  -- Optional
    --
    --  (Map) is not a designated and (Notch) is not specified,
    --  then 1 notch is set at last pin of rotor.
    --
    --  If (Map) is a designated rotor (ex: "I" or "BETA"), then mapping and notches
    --  are set using preset mapping for specific Enigma model. (Notch) is ignored.
    --
    --  Wiring_Error will be raised if designated rotor does not match available
    --  options for selected Enigma model.
    --
    --  Wiring_Error will be raised if user-specified map does not match rotor
    --  pins (ex: specifying 28 letters for 26 pin rotor).
    --
    --  Rotor_Error will be raised if selected rotor does not fit selected slot
    --  (ex: selecting GAMMA rotor for any slot other than 4 in M4 machine).
    --
    Enigma.Set_Rotor (M => Machine,
                      Slot => [number],
                      Pos  => [character],
                      Ring => [character]);
    --
    --      (Pos => [character])
    --          Set rotor position so [character] is shown in display.
    --          Ring is unchanged.
    --
    --      (Ring => [character])
    --          Set Ring so [character] is over rotor pin 1.
    --          Rotor is unchanged.
    --
    --      (Pos => [character], Ring => [character])
    --          First, set (Ring), then,  set (Pos).
    --

    --  For models that do not have a patch panel, this is a noop procedure.
    Enigma.Set_Patch (M => Machine,
                      S => [string]);      --  Set patch panel letter pair(s)

    -- Return a string showing all rotor displays
    S := Enigma.Get_Display (M => Machine);

    --  Encrypt() will automatically step rotors before each character is encrypted.
    C := Enigma.Encrypt (M => Machine,
                         C => [letter]);       --  Character to encrypt/decrypt
    S := Enigma.Encrypt (M => Machine,
                         S => [string]);       --  String to encrypt/decrypt

```

There were 2 styles of Enigma machines created - first was the printer style
(Handel, Schreib, H), then the Glühlampenchiffriermaschine (glow lamp cipher machine machine).

Models of Enigma machines expected to be emulated are:

| Model Name      | Notes |
| :-------------- | :---- |
| A               |  |
| Abwehr_1        | Also known as G |
| Abwehr_2        | Included printer socket |
| Abwehr_3        | Included plubboard |
| B               |  |
| B2              | Swedish General Staff (SGS) |
| C               |  |
| D               |  |
| Funkschlussel1  | Modified C - SGS with unique ring engravings |
| Funkschlussel2  | Modified C - SGS with standard ring engravings |
| G               | Modified Zahlwerk |
| IA              | Heer (Army), Luftwaffe (Air Force) with 3 rotors before 1938-12-15 |
| IB              | Heer (Army), Luftwaffe (Air Force) with 5 rotors since  1938-12-15 |
| K               |  |
| KD              | K with field rewirable UKW known as UKW-D (Dora) |
| M1              | Modified I - Kriegsmarine (Navy) |
| M2              | Modified I - Kriegsmarine (Navy) |
| M3              | Modified I - Kriegsmarine (Navy) |
| M4              | Modified I - Kriegsmarine (Navy U-Boats/Battleships) |
| Norway          | Modified I |
| Swiss           | Modified K |
| T               | Modified K code named Tirpitz (Japanese) |
| Uhr             | Modified I - Luftwaffe (Army) |
| Zahlwerk        | Modified D |
